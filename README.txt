## To clean:
ant -buildfile src/build.xml clean

----------------------------------------------------------
## To compile:
ant -buildfile src/build.xml all

Please compile before run. Needed to get BUILD and
BUILD/classes directories in directory structure
----------------------------------------------------------
## To run by specifying arguments from command line
ant -buildfile src/build.xml run -Darg0='path/to/input.txt' -Darg1='path/to/output.txt'

----------------------------------------------------------
## To create tarball for submission
ant -buildfile src/build.xml tarzip

----------------------------------------------------------
## Academic Honesty Statement

"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense."

[Date: ] -- 9/15/2017

----------------------------------------------------------
## Data Structure Justification

Insertion/Removal - O(n) avg/worst case. Data structure looks through values one
by one for indicated value, then inserts/removes value

Lookup - O(n) avg/worst case. Again function looks up value and may have to search entire array

Sum/Size - O(1) Adjusted during insertion/removal. Removes unnecessary calculations
for user.

----------------------------------------------------------
## Citations

FileProcessor.java (line 22) - https://docs.oracle.com/javase/7/docs/api/java/io/BufferedReader.html
Results.java (line 27) - https://docs.oracle.com/javase/7/docs/api/java/io/BufferedWriter.html
Results.java (line 11/24) - https://docs.oracle.com/javase/tutorial/collections/interfaces/map.html
